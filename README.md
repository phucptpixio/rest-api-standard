## Apendix

- [What is REST](#markdown-header-what-is-rest)
- [REST constraints](#markdown-header-rest-constraints)
- [REST Resource Naming Guide](#markdown-header-rest-resource-naming-guide)
    - [Use nouns to represent resources](#markdown-header-use-nouns-to-represent-resources)
    - [Keep it consistent](#markdown-header-keep-it-consistent)
        * [document](#markdown-header-1.-document)
        * [collection](#markdown-header-2.-collection)
        * [store](#markdown-header-3.-store)
        * [controller](#markdown-header-4.-controller)
    - [Never use CRUD function name in URIs](#markdown-header-never-use-crud-function-name-in-uris)
    - [Use query component to filter URI collection](#markdown-header-use-query-component-to-filter-uri-collection)
- [Versioning](#markdown-header-versioning)
- [Caching](#markdown-header-versioning)

### What is REST

REST is short for __RE__presentational __S__tate __T__ransfer, itself is an architect design, meaning it contain a sets of __rules__ and __principles__ to aid developers when creating their API. REST was introduced in 2000 by Roy Thomas Fielding ( who aslo the creator of HTTP ).


---

### REST Constraints

REST consists of [6 guiding constrains](https://restfulapi.net/rest-architectural-constraints) which must be satisfied if an interface needs to be referred as __RESTful__. 


1. __Client-server architecture__
    * This essentially means that __client application__ and __server application__ MUST be able to evolve separately without any dependency on each other. A client should know only __resource URIs__, and that’s all. Today, this is normal practice in web development, so nothing fancy is required from your side. Keep it simple.
2. __Statelessness (stateless)__
    * Each request from client to server must __contain all of the information necessary__ to understand the request, and cannot take advantage of any stored context on the server. Session state is therefore kept entirely on the client.
3. __Cachable__
    * clients and intermediaries can cache responses. Responses must, __implicitly or explicitly, define themselves as either cacheable or non-cacheable__ to prevent clients from becoming stale or inappropriate data in response to further requests. Well-managed caching partially or completely eliminates some client-server interactions, further improving scalability and performance.
4. __Layered system__
    * A client __cannot ordinarily tell__ whether it is connected directly to the end server, or to an intermediary along the way. If a proxy or load balancer is placed between the client and server, it won't affect their communications and there won't be a need to update the client or server code. Intermediary servers can improve system scalability by enabling load balancing and by providing shared caches. Also, security can be added as a layer on top of the web services (__middlewares__), and then clearly separate business logic from security logic. Adding security as a separate layer enforces security policies. Finally, it also means that a server can call multiple other servers to generate a response to the client.
5. __Uniform interface__
    * The overall system architecture must be __consistent__. In order to achieve the consistency, REST follow 4 interface principle:
        - __Identification of resources__ [1]
        - __Manipulation of resources through representations__ [2]
        - __Self-descriptive messages__ [3]
        - __Hypermedia as the engine of application state__ [4]
6. __Code on demand (optional)__
    * You can send a __piece of code to be executed__, like a small javascript to get widget from cnd... You shouldn't rely on this to run something important.

--- 


### REST Resource Naming Guide


* A resource can be a __singleton__ or a __collection__. For example, `“customers”` is a collection resource and `“customer”` is a singleton resource (in a banking domain). We can identify `“customers”` collection resource using the URI `“/customers”`. We can identify a single `“customer”` resource using the URI `“/customers/{customerId}”`.

* A resource may contain sub-collection resources also. For example, sub-collection resource `“accounts”` of a particular `“customer”` can be identified using the URN `“/customers/{customerId}/accounts”` (in a banking domain). Similarly, a singleton resource `“account”` inside the sub-collection resource `“accounts”` can be identified as follows: `“/customers/{customerId}/accounts/{accountId}”`.

* The key principles of REST involve separating API into logical __resources__. These __resources__ are manipulated using HTTP requests where the method (GET, POST, PUT, PATCH, DELETE) has specific meaning.


#### Use nouns to represent resources

- Resource should be __nouns__ (not __verbs__) that make sense from the perspective of the API consumer. Although the interal models may map neatly to resources, it isn't necessarily a one-to-one mapping. 
- The key is not leak irrelevant implementation details out.

__Example:__ let's take a look at the device management services. They will have __devices__, __user__ and __account__ (VIP and poor people). You can see __ticket__, __user__, __group__ are **nouns** and their can be consider as __resources__.

Once you have your __resources defined__, you need to identify what __actions__ apply to them and how those would map to your API. RESTful principles provide strategies to handle CRUD actions using HTTP methods mapped as follows:

* `GET /devices` - Retrieves a list of devices
* `GET /devices/12` - Retrieves a specific devices
* `POST /devices` - Creates a new devices
* `PUT /devices/12` - Updates devices #12
* `PATCH /devices/12` - Partially updates devices #12
* `DELETE /devices/12` - Delete devices #12

_(you shouldn't leave '/' at the end of the url, they are call trailling leader)_

* `GET /devices/` - bad usecase, because some browser or framework miss interpret the url and may lead to invalid.


For more clarity let's divide the __resource__ into four categories (document, collection, store and controller) and then __you should always target to put a resource into one archetype and then use it's naming convention consistently__. For _uniformity's sake_, _resist the temptation to design resources that are hybrids fo more than one archetype_.

### document

A document resource is a singular concept that is akin to an object instance or database record. In REST, you can view it as a single resource inside resource collection. A document’s state representation typically includes both fields with values and links to other related resources.

Use “singular” name to denote document resource archetype.

```
http://api.example.com/device-management/managed-devices/{device-id}
http://api.example.com/user-management/users/{id}
http://api.example.com/user-management/users/admin

```
### collection
A collection resource is a server-managed directory of resources. Clients may propose new resources to be added to a collection. However, it is up to the collection to choose to create a new resource or not. A collection resource chooses what it wants to contain and also decides the URIs of each contained resource.

Use “plural” name to denote collection resource archetype.
```
http://api.example.com/device-management/managed-devices
http://api.example.com/user-management/users
http://api.example.com/user-management/users/{id}/accounts
```

### store
A store is a client-managed resource repository. A store resource lets an API client put resources in, get them back out, and decide when to delete them. A store never generates new URIs. Instead, each stored resource has a URI that was chosen by a client when it was initially put into the store.

Use “plural” name to denote store resource archetype.

```
http://api.example.com/cart-management/users/{id}/carts
http://api.example.com/song-management/users/{id}/playlists
```
### controller
A controller resource models a procedural concept. Controller resources are like executable functions, with parameters and return values; inputs and outputs.

Use “verb” to denote controller archetype.
```
http://api.example.com/cart-management/users/{id}/cart/checkout
http://api.example.com/song-management/users/{id}/playlist/play
```

--- 

## Keep it consistent

Use consistent resource naming conventions and URI formatting for minimum ambiguily and maximum readability and maintainability. Below are some design hints to achieve consistency:

#### Use forward slash (/) to indicate hierachial relationships

The forward slash (/) character is used in the path portion of the URI to indicate a hierarchical relationship between resources. e.g.

```
http://api.example.com/device-management
http://api.example.com/device-management/managed-devices
http://api.example.com/device-management/managed-devices/{id}
http://api.example.com/device-management/managed-devices/{id}/scripts
http://api.example.com/device-management/managed-devices/{id}/scripts/{id}
```

#### Do not use trailing forward slash (/) in URIs

As the last character within a URI’s path, a forward slash (/) adds no semantic value and may cause confusion. It’s better to drop them completely.

```
http://api.example.com/device-management/managed-devices/
http://api.example.com/device-management/managed-devices 	/*This is much better version*/

```

#### Use hyphens (-) to improve the readability of URIs

To make your URIs easy for people to scan and interpret, use the hyphen (-) character to improve the readability of names in long path segments.

```
http://api.example.com/inventory-management/managed-entities/{id}/install-script-location  //More readable
http://api.example.com/inventory-management/managedEntities/{id}/installScriptLocation  //Less readable
```

#### Do not use underscores (_)

It’s possible to use an underscore in place of a hyphen to be used as separator – But depending on the application’s font, it’s possible that the underscore ( _ ) character can either get partially obscured or completely hidden in some browsers or screens.

To avoid this confusion, use __hyphens__ (-) instead of _underscores_ ( _ ).

```
http://api.example.com/inventory-management/managed-entities/{id}/install-script-location  //More readable
http://api.example.com/inventory_management/managed_entities/{id}/install_script_location  //More error prone
```

#### Use lowercase letters in URIs

When convenient, lowercase letters should be consistently preferred in URI paths.

[RFC 3986](https://tools.ietf.org/html/rfc3986) defines URIs as case-sensitive except for the scheme and host components. e.g.

```
http://api.example.org/my-folder/my-doc  //1
HTTP://API.EXAMPLE.ORG/my-folder/my-doc  //2
http://api.example.org/My-Folder/my-doc  //3

```

In above examples, 1 and 2 are same but 3 is not as it uses __My-Folder__ in capital letters.

#### Do not use file extentions

File extensions look bad and do not add any advantage. Removing them decreases the length of URIs as well. No reason to keep them.

Apart from above reason, if you want to highlight the media type of API using file extenstion then you should rely on the media type, as communicated through the `Content-Type` header, to determine how to process the body’s content.

```
http://api.example.com/device-management/managed-devices.xml  /*Do not use it*/
http://api.example.com/device-management/managed-devices 	/*This is correct URI*/
```

### Never use CRUD function name in URIs

URIs should not be used to indicate that a CRUD function is performed. URIs should be used to uniquely identify resources and not any action upon them. HTTP request methods should be used to indicate which CRUD function is performed.

```
GET http://api.example.com/device-management/listAllDevices     /*Do not use it*/
GET http://api.example.com/device-management/managed-devices    

POST http://api.example.com/device-management/addNewDevice   /*Do not use it*/
POST http://api.example.com/device-management/managed-devices
```

### Use query component to filter URI collection 

Many times, you will come across requirements where you will need a collection of resources sorted, filtered or limited based on some certain resource attribute. For this, do not create new APIs – rather enable sorting, filtering and pagination capabilities in resource collection API and pass the input parameters as query parameters. e.g.

```
http://api.example.com/device-management/managed-devices
http://api.example.com/device-management/managed-devices?region=VN
http://api.example.com/device-management/managed-devices?region=VN&brand=XYZ
http://api.example.com/device-management/managed-devices?region=VN&brand=XYZ&sort=installation-date

```

--- 

## Versioning

Always version your API. Versioning helps you iterate faster and prevents invalid requests from hitting updated endpoints. It also helps smooth over any major API version transitions as you can continue to offer old API versions for a period of time.

#### When to Version

APIs only need to be up-versioned when a breaking change is made. Breaking changes include: 

* a change in the format of the reponse data for one or more calls
* a change in the response type (i.e changing an integer to a float)
* removing any part of the API

__Breaking changes__ should always result in a change in the major version for an API or content [response type](https://www.iana.org/assignments/media-types/media-types.xhtml).

#### URI Versioning

Using the URI is the most straightforward approach (and most commonly used as well) though it does violate the principle that a URI should refer to a unique resource. You are also guaranteed to break client integration when a version is updated.

```
http://api.example.com/v1
http://apiv1.example.com
```
The version need not be numeric, nor specified using the “v[x]” syntax. Alternatives include dates, project names, seasons or other identifiers that are meaningful enough to the team producing the APIs and flexible enough to change as the versions change.

* [Some URI design debates for URI versioning](https://stackoverflow.com/questions/389169/best-practices-for-api-versioning)
* [Stripe's approach to API versioning](https://stripe.com/docs/api/versioning)

## Content Negotiation

Generally, resources can have multiple presentations, mostly because there may be multiple different clients expecting different representations. Asking for a suitable presentation by a client, is referred as content negotiation.

1. Content negotiation using HTTP headers (recommended)

At server side, an incoming request may have an entity attached to it. To determine it’s type, server uses the HTTP request header Content-Type. Some common examples of content types are “text/plain”, “application/xml”, “text/html”, “application/json”, “image/gif”, and “image/jpeg”.

`Content-Type: application/json`

Similarly, to determine what type of representation is desired at client side, HTTP header ACCEPT is used. It will have one of the values as mentioned for Content-Type above.

`Accept: application/json`


Normally, if no `Accept` header is present in the request, the server can send pre-configured default representation type.


2. Content negotiation using URL patterns
Another way to pass content type information to server, client may use specific extension in resource URIs. For example, a client can ask for details using:

```
http://rest.api.com/v1/employees/20423.xml  /* return xml */
http://rest.api.com/v1/employees/20423.json /* return json */
```

We can take a look at [how Laravel handle json response ](https://laravel.com/docs/6.x/responses#json-responses)

- The `json` method will automatically se the `Content-Type` header to `application/json`, as well as convert the given array to JSON using the `json_encode` PHP function:

```php
return response()->json([
    'name' => 'Abigail',
    'state' => 'CA'
]);
```

## Caching

`GET request` should be cachable by default - until special condition arises. Usually browser treat all GET requests cachable. POST requests are not cacheable by default but can be made cacheable if either an `Expires` header or a `Cache-Control` header with a directive, to explicity allows caching, is added to the response. Responses to `PUT` and `DELETE` requests are not cacheable at all.

There are two main HTTP response headers that we can use to control caching behavior: 

#### Expires

The Expires HTTP header specifies an absolute expiry time for a cached representation. Beyond that time, a cached representation is considered stale and must be re-validated with the origin server. To indicate that a representation never expires, a service can include a time up to one year in the future.

```
Expires: Fri, 20 May 2016 19:20:49 IST
```

#### Cache-Control

The header value comprises one or more comma-separated [ directives ](https://tools.ietf.org/html/rfc7234#page-24). These directives determine whether a response is cacheable, and if so, by whom, and for how long e.g. `max-age` or `s-maxage` directives.

```
Cache-Control: max-age=3600
```

